#! /bin/sh
echo ----======== ENV VARIABLES =======------
echo APP_HOST: $APP_HOST or default
echo CLIENT_ID: $CLIENT_ID or default
echo CLIENT_SECRET: $CLIENT_SECRET or default
echo ----======== RUN TESTS =======------
cd dist
mvn clean install

