package base;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;

import static io.restassured.RestAssured.given;

public class BaseApiTests {
    private String client_id = System.getProperty("client.id");
    private String client_secret = System.getProperty("client.secret");
    @BeforeClass
    public void setup(){
        RestAssured.baseURI = System.getProperty("app.host");
    }


    public String getToken() {
        String token =  given().
                param("grant_type", "password").
                param("username", "einsight@quby").
                param("password", "Qwerty1234").
                param("client_id", client_id).
                param("client_secret", client_secret).
                param("tenant_id", "eneco-acc").
                when().
                post("/token").
                then().
                statusCode(200).
                and().extract().path("access_token");
        return token;
    }

    public String getAgreementId(){
               return auth().
                when().
                get("/toon/v3/agreements").
                then().extract().path("[0].agreementId");
    }

    public RequestSpecification auth() {
        return given().
                auth().preemptive().oauth2(getToken()).contentType(ContentType.JSON);
    }


}
