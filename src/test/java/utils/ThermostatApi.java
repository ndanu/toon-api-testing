package utils;

import base.BaseApiTests;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import data.ThermostatBody;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class ThermostatApi extends BaseApiTests {


    public void updateThermostatSetPoint(Integer setpoint) {
        ThermostatBody putBody = new ThermostatBody();
        putBody.setCurrentSetpoint(setpoint);
        putBody.setRealSetpoint(setpoint);
        GsonBuilder builder = new GsonBuilder();
        Gson jsonBody = builder.create();
                auth()
                .when()
                .body(jsonBody.toJson(putBody))
                .put("/toon/v3/"+ getAgreementId()+"/thermostat")
                .then().statusCode(200);

    }

    public Response getThermostatDetails(){
        return auth().
                when().
                get("/toon/v3/"+ getAgreementId()+"/thermostat").
                then().statusCode(200).log().status()
                .body(matchesJsonSchemaInClasspath("thermostat/thermostat-schema.json"))
                .extract().response();
    }

    public ValidatableResponse getThermostatStates(){
        return auth().
                when().
                get("toon/v3/"+ getAgreementId()+"/thermostat/states")
                .then().statusCode(200).log().status()
                .body(matchesJsonSchemaInClasspath("thermostat/thermostat-states-schema.json"));
    }

}
