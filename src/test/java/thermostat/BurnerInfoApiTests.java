package thermostat;

import base.BaseApiTests;
import io.restassured.response.Response;
import org.testng.Assert;

import org.testng.annotations.Test;
import utils.ThermostatApi;


public class BurnerInfoApiTests extends BaseApiTests {
    ThermostatApi thermostatApi = new ThermostatApi();

    @Test
    public void checkBoilerInfoWhenSetpointIsHigherThanRoomTemp(){

        Response initialThermostatDetails = thermostatApi.getThermostatDetails();
        Integer currentRoomTemperature = initialThermostatDetails.body().path("currentDisplayTemp");

        thermostatApi.updateThermostatSetPoint(currentRoomTemperature + 100);
        Response res = thermostatApi.getThermostatDetails();

        String burnerInfo = res.body().path("burnerInfo");
        Assert.assertEquals(burnerInfo,"1","Boiler should be off when set point is below room temperature");
    }

    @Test
    public void checkBoilerInfoWhenSetpointIsLowerThanRoomTemp(){
        Response initialThermostatDetails = thermostatApi.getThermostatDetails();
        Integer currentRoomTemperature = initialThermostatDetails.body().path("currentDisplayTemp");

        thermostatApi.updateThermostatSetPoint(currentRoomTemperature - 100);
        Response res = thermostatApi.getThermostatDetails();

        String burnerInfo = res.body().path("burnerInfo");
        Assert.assertEquals(burnerInfo,"0","Boiler should be off when set point is below room temperature");
    }

    @Test
    public void checkBoilerInfoWhenSetpointIsEqualToRoomTemp(){
        Response initialThermostatDetails = thermostatApi.getThermostatDetails();
        Integer currentRoomTemperature = initialThermostatDetails.body().path("currentDisplayTemp");

        thermostatApi.updateThermostatSetPoint(currentRoomTemperature);
        Response res = thermostatApi.getThermostatDetails();

        String burnerInfo = res.body().path("burnerInfo");
        System.out.printf("Requirements are not clear for this scenario. Anyway burnerInfo is: " + burnerInfo +"\n");
    }

}
