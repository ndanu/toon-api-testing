package thermostat;

import base.BaseApiTests;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ThermostatApi;


public class SetPointApiTests extends BaseApiTests {
    ThermostatApi thermostatApi = new ThermostatApi();

    @Test
    public void userIsAbleToAdjustSetPointValueBetween6And30(){
        thermostatApi.updateThermostatSetPoint(2500);

        Response res = thermostatApi.getThermostatDetails();
        Integer currentSetpoint = res.body().path("currentSetpoint");
        Assert.assertEquals(currentSetpoint.intValue(),2500,"user Is Able To Adjust SetPoint Between 6 and 30");
    }
    @Test
    public void userIsAbleToAdjustSetPointValueToMax30(){
        thermostatApi.updateThermostatSetPoint(3000);

        Response res = thermostatApi.getThermostatDetails();
        Integer currentSetpoint = res.body().path("currentSetpoint");
        Assert.assertEquals(currentSetpoint.intValue(),3000,"user Is Able To Adjust SetPoint To Max 6");
    }
    @Test
    public void userIsNotAbleToAdjustSetPointOverMax30(){
        thermostatApi.updateThermostatSetPoint(3100);

        Response res = thermostatApi.getThermostatDetails();
        Integer currentSetpoint = res.body().path("currentSetpoint");
        Assert.assertNotEquals(currentSetpoint.intValue(),3100,"user Is Not Able To Adjust SetPoint Over Max 30");
    }
    @Test
    public void userIsAbleToAdjustSetPointValueToMin6(){
        thermostatApi.updateThermostatSetPoint(600);

        Response res = thermostatApi.getThermostatDetails();
        Integer currentSetpoint = res.body().path("currentSetpoint");
        Assert.assertEquals(currentSetpoint.intValue(),600,"user Is Able To Adjust SetPoint To Min 6");
    }
    @Test
    public void userIsNotAbleToAdjustSetPointBellowMin6(){
        thermostatApi.updateThermostatSetPoint(500);

        Response res = thermostatApi.getThermostatDetails();
        Integer currentSetpoint = res.body().path("currentSetpoint");
        Assert.assertNotEquals(currentSetpoint.intValue(),500,"user Is Not Able To Adjust SetPoint Bellow Min 6");
    }


}
