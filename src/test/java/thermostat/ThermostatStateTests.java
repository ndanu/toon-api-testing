package thermostat;

import base.BaseApiTests;
import org.testng.annotations.Test;
import utils.ThermostatApi;

import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class ThermostatStateTests extends BaseApiTests{
    ThermostatApi thermostatApi = new ThermostatApi();

    @Test
    public void getThermostatStates200() {
        thermostatApi.getThermostatStates();
    }

    @Test
    public void getThermostatStatesWhenIncorrectAgreementID403() {
        auth()
                .when()
                .get("toon/v3/incorrectAgreement/thermostat/states")
                .then().statusCode(403)
                .body(matchesJsonSchemaInClasspath("not-authorized-error-schema.json"));
    }

    @Test
    public void getThermostatStatesWhenNoAuth401() {
        when()
                .get("/toon/v3/"+ getAgreementId()+"/thermostat/states")
                .then()
                .statusCode(401)
                .body(matchesJsonSchemaInClasspath("invalid-access-token-schema.json"));
    }
}
