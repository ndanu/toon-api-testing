package data;

public class ThermostatBody {
    private Integer currentDisplayTemp = 0;
    private Integer currentSetpoint = 0;
    private Integer realSetpoint = 0;
    private Integer programState = 0;
    private Integer activeState = -1;
    private Integer nextProgram = -1;
    private Integer nextState = -1;
    private Integer nextTime = 0;
    private Integer nextSetpoint = 0;
    private Integer errorFound = 0;
    private Integer boilerModuleConnected = 0;
    private String burnerInfo = "";
    private String otCommError = "";
    private Integer currentModulationLevel = 0;

    public ThermostatBody setCurrentDisplayTemp(Integer currentDisplayTemp) {
        this.currentDisplayTemp = currentDisplayTemp;
        return this;
    }

    public ThermostatBody setRealSetpoint(Integer realSetpoint) {
        this.realSetpoint = realSetpoint;
        return this;
    }
    public ThermostatBody setCurrentSetpoint(Integer currentSetpoint) {
        this.currentSetpoint = currentSetpoint;
        return this;
    }

    public ThermostatBody setProgramState(Integer programState) {
        this.programState = programState;
        return this;
    }

    public ThermostatBody setActiveState(Integer activeState) {
        this.activeState = activeState;
        return this;
    }

    public ThermostatBody setNextProgram(Integer nextProgram) {
        this.nextProgram = nextProgram;
        return this;
    }

    public ThermostatBody setNextState(Integer nextState) {
        this.nextState = nextState;
        return this;
    }

    public ThermostatBody setNextTime(Integer nextTime) {
        this.nextTime = nextTime;
        return this;
    }

    public ThermostatBody setNextSetpoint(Integer nextSetpoint) {
        this.nextSetpoint = nextSetpoint;
        return this;
    }

    public ThermostatBody setErrorFound(Integer errorFound) {
        this.errorFound = errorFound;
        return this;
    }

    public ThermostatBody setBoilerModuleConnected(Integer boilerModuleConnected) {
        this.boilerModuleConnected = boilerModuleConnected;
        return this;
    }

    public ThermostatBody setBurnerInfo(String burnerInfo) {
        this.burnerInfo = burnerInfo;
        return this;
    }

    public ThermostatBody setOtCommError(String otCommError) {
        this.otCommError = otCommError;
        return this;
    }

    public ThermostatBody setCurrentModulationLevel(Integer currentModulationLevel) {
        this.currentModulationLevel = currentModulationLevel;
        return this;
    }
}

