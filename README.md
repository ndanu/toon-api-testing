# Toon Testing APIs
Quby QA technical assesment

## Getting started
Bellow you can find the steps necessary to run the REST Assured tests on your local machine or using docker.

### Local
#### Prerequisites
* [Maven](https://maven.apache.org/)

#### Run tests
with default environment variables
```
~> cd toon-api-testing
~> mvn clean install
```

with custom environment variables
```
~> cd toon-api-testing
~> mvn clean install -Dapp.host={hostValue} -Dclient.id={cidValue} -Dclient.secret={csecretValue}
```

### Docker
#### Prerequisites
* [Docker](https://www.docker.com/get-docker)

#### Run tests

```
~> cd toon-api-testing
~> docker-compose up
```
To run it with custom environment variable open the docker-compose.yaml and update the environment variables.